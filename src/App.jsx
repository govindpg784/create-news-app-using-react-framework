import { useState } from 'react'

import './App.css'
import Header from './Header'


function App() {
  const [count, setCount] = useState(0)

  return (
    <>
     <Header />
     <main>
            <section className="section1">
           <div className="container">
           <div id="mainfirst">
        <h1>Wellcome to online super market</h1>
        <p>
          Discover a world of convenience at our online supermarket! Browse
          through a diverse selection of fresh produce, pantry staples, and
          household essentials from the comfort of your home. Enjoy seamless
          shopping, doorstep delivery, and exclusive deals—all at your
          fingertips. Elevate your shopping experience with us!
        </p>
      </div>
      <img id="add" src="/image1.jpeg" alt="" />
    </div>
  </section>
  <section className="section2">
    <div className="container">
      <h2>chose item</h2>
    </div>
    <div className="container">
      <img src="/product-img-4.jpg" alt="" />
      <img src="/product-img-5.jpg" alt="" />
      <img src="/product-img-6.jpg" alt="" />
    </div>
  </section>
</main>
<footer></footer>
    </>
  )
}

export default App
